﻿namespace ShippingCostApp
{
    public sealed class PaymentAmount
    {
        public decimal Amount(int up, int qty, bool mem)
        {
            decimal a = up * qty;
            if (mem)
            {
                if (a < 3000) a += 500;
            }
            else
            {
                if (a < 10000) a += 500;
            }

            return a * 1.08m;
        }
    }
}